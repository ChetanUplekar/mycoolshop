import APIServices from '../../services/APIServices'
import APIservices from '../../services/APIServices'
import {ALL_PRODUCT, PRODUCT_BY_COLOR, PRODUCT_BY_MATERIAL, Featured_Product} from '../actionTypes/actionTypes'

export const getAllproductsAction = () => {
    return (dispatch) => {
        APIservices.getAllproducts()
            .then(res => {
                dispatch(getAllproducts(res.data.products));
            })
            .catch()
    }
}

export const getAllColorAction = () => {
    return(dispatch) => {
        APIservices.getProductByColor()
        .then(res=>{
            dispatch(getProductByColor(res.data.colors))
        })
    }
}

export const getAllMaterialAction = () => {
    return(dispatch) => {
        APIservices.getProductByMaterial()
        .then(res=>{
            dispatch(getProductByMaterial(res.data.material))
        })
    }
}

export const getFeaturedProductAction = () => {
    return(dispatch) => {
        APIServices.getFeaturedProduct()
        .then(res=>{
            dispatch(getFeaturedProduct(res.data.featured))
        })
    }
}


export const getAllproducts = (data)=> {
    return{
        type: ALL_PRODUCT,
        payload: data
    }
}

export const getProductByColor = (data)=> {
    return{
        type: PRODUCT_BY_COLOR,
        payload: data
    }
}

export const getProductByMaterial = (data) => {
    return{
        type:PRODUCT_BY_MATERIAL,
        payload: data
    }
}

export const getFeaturedProduct = (data) => {
    return{
        type:Featured_Product,
        payload: data
    }
}
