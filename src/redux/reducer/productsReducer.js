import {ALL_PRODUCT, PRODUCT_BY_COLOR , PRODUCT_BY_MATERIAL, Featured_Product} from '../actionTypes/actionTypes'

const initialState = {
    products: [],
    colors: [],
    materials: [],
    AllfeaturedProduct: [],
}


export const productsReducer = (state = initialState, action) => {
    switch(action.type){
        
        case ALL_PRODUCT : {
            return {
                ...state,
                products: action.payload,
            }
        }

        case PRODUCT_BY_COLOR : {
            return {
                ...state,
                colors: action.payload,
            }
        }

        case PRODUCT_BY_MATERIAL : {
            return {
                ...state,
                materials: action.payload,
            }
        }

        case Featured_Product : {
            return {
                ...state,
                AllfeaturedProduct: action.payload
            }
        }

        default: {
            return {
                ... state
            }
        }


}
}