import axios from 'axios';
import {Base_Url,Allproducts,AllproductsByColor,AllproductsByMaterial,FeaturedProduct} from '../resources/APIEndpoints'

function APIServices() {
    this.getAllproducts = () => axios.get(Base_Url + Allproducts)
    this.getProductByColor = () => axios.get(Base_Url + AllproductsByColor)
    this.getProductByMaterial = () => axios.get(Base_Url + AllproductsByMaterial)
    this.getFeaturedProduct = () => axios.get(Base_Url+ FeaturedProduct)
}

export default new APIServices();
