import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {getAllproductsAction, getAllColorAction, getAllMaterialAction} from '../../redux/action/productsAction'
import '../../css/style.css'

export default function Siderbar({colors, materials, FilteredData, Idnew}) {
    const dispatch = useDispatch();
    const [FilterData, setFilterData] = useState({colorId: null, materialId: null})

    const filter = (type, id) =>{
        if (type === 'color') {
            setFilterData({colorId: id});
        }else {
            setFilterData({materialId: id});
        }
        FilteredData(type, id);
    }

    // let colors = useSelector((state) => {
    //     return state.product.colors;
    // });

    // let materials = useSelector((state)=>{
    //     return state.product.materials
    // })

    useEffect(() => {
        async function getallproducts() {
          dispatch(getAllColorAction());
          dispatch(getAllMaterialAction())
        }
        getallproducts();
      }, []);

    return (
        <div>
            <div className="sidebar">
                <div className="tags mt-3 col-sm-6">
                    <h6>Colors</h6>
                    <p className="mb-2" onClick={()=>Idnew(null)}>All</p>
                    {colors.map((ele)=>{
                        return(
                            <p className="mb-2" onClick={()=>filter('colors', ele.id)}>{ele.name}</p>
                        )
                    })}
                </div>
                <div className="tags mt-3">
                    <h6>Materials</h6>
                    <p className="mb-2" onClick={()=>Idnew(null)}>All</p>
                    {materials.map((ele)=>{
                        return(
                            <p className="mb-2" onClick={()=>filter('materials', ele.id)}>{ele.name}</p>
                        )
                    })}
                </div>

                <div className="tags mt-3">
                    <h6>Tags</h6>
                    <p className="mb-2">All</p>
                    <p className="mb-2">Genric</p>
                    <p className="mb-2">Genric</p>
                </div>

            </div>
        </div>
        // <div>
        //     <div className="sidebar">
        //         <div className="Tags my-4">
        //             <h6>Tags</h6>
        //             <div className="my-2 ml-3">
        //                 <button>All</button>
        //             </div>
        //             <div className="my-2">
        //                 <button>Genric</button>
        //             </div>
        //             <div className="my-2">
        //             <button>Genric</button>
        //             </div>
        //             <div className="my-2">
        //                 <button>Genric</button>
        //             </div>
        //         </div>

        //         <div className="Tags my-4">
        //             <h6>Materials</h6>
        //             <button onClick={()=>Idnew(null)}>All</button>
        //             {!isLoading ? (
        //                 (materials.map((ele)=>{
        //                     return(
        //                         <div className="mt-2">
        //                             <button onClick={()=>Idnew(ele.id)}>{ele.name}</button>
        //                         </div>
        //                     )
        //                 }))
        //             ) : (
        //                 <p>wait...</p>
        //             )}
        //         </div>

        //         <div className="Tags my-4">
        //             <h6>colors</h6>
        //             <button onClick={()=>Idnew(null)}>All</button>
        //             {!isLoadingOne ? (
        //                 (colors.map((ele)=>{
        //                     return(
        //                         <div className="mt-2">
        //                             <button onClick={()=>Id(ele.id)}>{ele.name}</button>
        //                         </div>
        //                     )
        //                 }))
        //             ) : (
        //                 <p>wait...</p>
        //             )}
        //         </div>

        //     </div>
        // </div>


    )
}
