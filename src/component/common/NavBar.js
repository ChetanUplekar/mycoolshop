import React, { useEffect, useState } from "react";
import '../../css/style.css'

export default function NavBar({onFilterChange, count}) {
    const[isclickAll, setClickAll] = useState(false)
    const[isclickFeatured, setClickFeatured] = useState(false)

    const onChange = (show) => {

        setClickAll(show=== 'show_all' ? true : false)
        setClickFeatured(show === 'show-all' ? false : true)

        onFilterChange(show);
    }

    return (
        <div className="container-fluid px-0">
            <div className="row my-3 text-center">
                <h6>MYCOOLSHOP.COM</h6>
            </div>
            <div className="bg-light">
                    <div className="d-flex justify-content-between px-5 py-3 align-items-center">
                        <div>
                            <span className="mx-2" onClick={()=>onChange('show_all')}>All Products</span>
                            <span className="mx-2" onClick={()=>onChange('show_featured')}>Featured Product</span>
                        </div>
                        <div className="d-flex align-items-center">
                            <div><i class="fa fa-shopping-cart fa-2x"></i></div> 
                            <div className="mx-2"><h6>{count}</h6></div>
                        </div>
                    </div>
                </div> 
        </div>
    )
}
