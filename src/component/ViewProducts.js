import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {getAllproductsAction, getAllColorAction, getAllMaterialAction, getFeaturedProductAction} from '../redux/action/productsAction'
import Siderbar from "./common/Sidebar";
import NavBar from './common/NavBar'
import '../css/style.css'
  
export default function ViewProducts() {
    const dispatch = useDispatch();
    const[result, setResult] = useState([])
    const[count,setCount] = useState(12)

    let products = useSelector((state) => {
        return state.product.products;
    });

    let colors = useSelector((state) => {
        return state.product.colors;
    });

    let materials = useSelector((state)=>{
        return state.product.materials
    })

    useEffect(() => {
        async function getallproducts() {
          dispatch(getAllproductsAction());
          dispatch(getFeaturedProductAction())
        }
        getallproducts();
      }, []);

    
    
    const FilteredData = (type, id) =>{
        console.log(id)
        let result=[]
        if (type === 'colors') {
            result = products.filter(ele => ele.colorId === id)
        }else if(type === 'materials'){
            result = products.filter(ele => ele.materialId === id)
        }
        console.log(result)
        setResult(result)
    }

    useEffect(() => {
        if (products && products.length > 0) {
          setResult(products);
        }
      }, [products]);

      let featuredProduct = useSelector((state)=>{
        return state.product.AllfeaturedProduct;
    })
      

    const onFilterChange = (show) =>{
        if (show === 'show_all') {
            setResult(products)
        }else if(show === 'show_featured'){
            let result = [];
            
            products.map(ele => {
                for (let index = 0; index < featuredProduct.length; index++) {
                  ele.id === featuredProduct[index].productId ? result.push(ele) : void(0)          
                }
            })
            setResult(result)
        }
    }

    const Countinc = () =>{
        setCount(count+1);
    }

    const all = (id) =>{
        let result = [];
        result = products.filter((ele)=>{
           if(id===null){
                return ele
           }else{
            return ele.materialId === id 
           }
        })
        setResult(result)
    }

    

    return (
        <div>
            <div className="row mx-0">
                <NavBar onFilterChange={onFilterChange} count={count}></NavBar>
            </div>
            <div className="row mx-0">
                <div className="col col-xl-2 col-lg-2 col-md-2 col-sm-12 mt-5">
                    <Siderbar Idnew={all} colors = {colors} materials = {materials} FilteredData={FilteredData}></Siderbar>
                </div>
                <div className="col col-xl-10 col-lg-10 col-md-10 col-sm-12 p-0">
                    <div className="row mt-4 mx-0 justify-content-center">
                        {result.map((ele)=>{
                            return(
                                // <div className="col col-3 card mx-2 my-3">
                                //     <div>
                                //         <img src={ele.image} className="img-fluid card-img-top"/>
                                //     </div>
                                //     <div className="m-2">
                                //         <h5>{ele.name}</h5>
                                //     </div>
                                //     <div className="m-2">
                                //         <p>INR {ele.price}</p>
                                //     </div>
                                // </div>
                                <div className="col col-lg-3 col-md-6 col-sm-12 card m-2">
                                    <div className="overlay-container">
                                        <img src={ele.image} className="card-img-top img-fluid"></img>
                                        <div className="overlay-text">
                                            <div className="text">
                                                <button onClick={()=>Countinc()} style={{'color': "#fff"}}>Add to Card</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="p-2">
                                        <h5>{ele.name}</h5>
                                    </div>
                                    <div className="px-2">
                                        <p>INR {ele.price}</p>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
}
