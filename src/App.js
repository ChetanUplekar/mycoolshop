import logo from './logo.svg';
import './App.css';
import Product from './component/ViewProducts'
import store from './redux/store/store'
import { Provider } from 'react-redux';
import Siderbar from './component/common/Sidebar';


function App() {
  return (
    <Provider store={store}>
    <div className="App">
        <Product />
    </div>
    </Provider>
  );
}

export default App;
